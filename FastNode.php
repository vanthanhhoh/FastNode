<?php
/**
 * Copyright (c) 10 / 2016
 * Author : ThanhNV
 * Email : vanthanhhoh@gmail.com
 */


define('FN_PATH', plugin_dir_path( __FILE__ ) );
define('FN_APP',FN_PATH.'app'.DIRECTORY_SEPARATOR);
define('FN_CORE',FN_PATH.'core'.DIRECTORY_SEPARATOR);

include_once FN_CORE.'Boot.php';
include_once FN_CORE.'Autoload.php';