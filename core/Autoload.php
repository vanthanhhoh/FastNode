<?php
/**
 * Copyright (c) 10 / 2016
 * Author : ThanhNV
 * Email : vanthanhhoh@gmail.com
 */

/**
 * Created by PhpStorm.
 * User: vanth
 * Date: 10/8/2016
 * Time: 12:12 AM
 */

if( !defined('FN_PATH') && !defined('FN_CORE') && !defined('FN_APP')) exit('Access defined');

function FnAutoload($ClassName)
{
    $filePath = FN_APP . $ClassName . DIRECTORY_SEPARATOR . $ClassName . '.php';
    if(file_exists($filePath) && is_readable($filePath))
        include $filePath;
}
function __autoload($classname)
{
    FnAutoload($classname);
}
